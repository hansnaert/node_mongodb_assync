//This file requires node > 7. ... because it uses async/await support

var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');

test();

async function test() {
    // 1. Connect to MongoDB instance running on localhost

    // Connection URL
    var url = 'mongodb://localhost:27017/test_assync';

    const db = await MongoClient.connect(url);
    console.log("Connected successfully to server");

    try {
        await insertDocuments(db);
        await findDocuments(db);
        await indexCollection(db);
        await aggregateDocuments(db);
    }
    catch(error)
    {
        console.log("exception occurred: "+error);
    }
    db.close();
}

// 2. Insert
async function insertDocuments(db) {
    //throw 400;
    const results = await db.collection('restaurants')
        .insertMany([
            { "name": "Sun Bakery Trattoria", "stars": 4, "categories": ["Pizza", "Pasta", "Italian", "Coffee", "Sandwiches"] },
            { "name": "Blue Bagels Grill", "stars": 3, "categories": ["Bagels", "Cookies", "Sandwiches"] },
            { "name": "Hot Bakery Cafe", "stars": 4, "categories": ["Bakery", "Cafe", "Coffee", "Dessert"] },
            { "name": "XYZ Coffee Bar", "stars": 5, "categories": ["Coffee", "Cafe", "Bakery", "Chocolates"] },
            { "name": "456 Cookies Shop", "stars": 4, "categories": ["Bakery", "Cookies", "Cake", "Coffee"] }
        ]);
    console.log(results)
    return results;
}

// 3. Query Collection
async function findDocuments(db) {
    // Get the documents collection
    const collection = db.collection('restaurants');
    const docs = await collection.find({}).toArray();
    console.log("Found the following records");
    console.log(docs)
    return docs;
}

// 4. Create Index
async function indexCollection(db) {
    const results = await db.collection('restaurants').createIndex(
        { "name": 1 },
        null
    );
    console.log(results);
    return results;
};

// 5. Perform Aggregation

async function aggregateDocuments(db) {
    const results = await db.collection('restaurants')
        .aggregate(
        [{ '$match': { "categories": "Bakery" } },
        { '$group': { '_id': "$stars", 'count': { '$sum': 1 } } }])
        .toArray();

    console.log(results)
    return results;
}